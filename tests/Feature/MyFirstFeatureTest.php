<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MyFirstFeatureTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testHomePage()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testCalcPage()
    {
        $array = array(
            "a" => "4",
            "b" => "0",
            "action" => "/",
        );
        $response = $this->post('/calc', $array);
        $response->assertStatus(200);
        $response->assertSee("don&#039;t divide by zero");
    }

}
